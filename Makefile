TARGET = main.out

HDRS = \
	   project/include

SRCS = \
       project/src/main.c \
       project/src/utils.c

.PHONY: all clean

all: $(SRCS)
	gcc -Wall -Wextra -Werror $(addprefix -I,$(HDRS)) -o $(TARGET) $(CFLAGS) $(SRCS)

clean:
	rm -rf $(TARGET)
